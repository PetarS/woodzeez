// ////////////////////////////////////////////////
// Required taskes
// gulp build
// bulp build:serve
// // /////////////////////////////////////////////

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minify = require('gulp-minify-css');
var merge = require('merge-stream');
var rename = require('gulp-rename');

// ////////////////////////////////////////////////
// Styles Tasks
// ///////////////////////////////////////////////

gulp.task('styles', function() {

    var scssStream = gulp.src([
            'assets/scss/style.scss'
        ])
            .pipe(sass())
            .pipe(concat('scss-files.scss'))
        ;

    var cssStream = gulp.src([
            'assets/bower_components/font-awesome/css/font-awesome.min.css',
            'assets/bower_components/slick-carousel/slick/slick.css',
            'assets/bower_components/slick-carousel/slick/slick.css',
            'assets/bower_components/jquery-ui/themes/base/slider.css',
        ])
            .pipe(concat('css-files.css'))
        ;

    var mergedStream = merge(scssStream, cssStream)
        .pipe(concat('style.css'))
        .pipe(minify())
        .pipe(gulp.dest('assets/css'))

    return mergedStream;
});

// ////////////////////////////////////////////////
// Styles Tasks
// ///////////////////////////////////////////////

var jsFiles = [
        'assets/bower_components/jquery/dist/jquery.min.js',
        'assets/bower_components/sidr/dist/jquery.sidr.min.js',
        'assets/bower_components/slick-carousel/slick/slick.js',
        'assets/no_bower_components/glasscase/js/modernizr.custom.js',
        'assets/bower_components/bLazy/blazy.min.js',
        'assets/js/theme.js'
    ],
    jsDest = 'assets/js';

gulp.task('scripts', function() {
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest));
});

// ////////////////////////////////////////////////
// Watch Tasks
// // /////////////////////////////////////////////

gulp.task ('watch', function(){
    gulp.watch('assets/scss/**/*.scss', ['styles']);
});


//gulp.task('default', ['scripts', 'scripts-foo', 'watch']);
gulp.task('default', ['styles', 'watch']);
